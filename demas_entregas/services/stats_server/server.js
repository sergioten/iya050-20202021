const { GraphQLServer } = require("graphql-yoga");
const fetch = require("node-fetch");

const fetchJson = (...args) => fetch(...args).then(response => response.json());


const typeDefs = `
  type Query {
    player(name: String!): player
    allGames(name: String): [game]
    allPlayers: [player]
  }
  type Mutation {
    creaplayer(name: String!): player!
    createGame(input: crearInputPlayer, map: [[Int!]!]!, x: Int!, y: Int!, direction: String!, lastPosition: Int!, catches: [String]): game!
    updatePlayer(input: crearInputPlayer): player!
    updateCatches(input: createGameInput, Catche: String!, url: String!): game!
  }
  
  input crearInputPlayer{
    name: String!
    gameId: ID!    
    games: [String]
    
  }  
  
  type player {
    name: String!
    gameId: ID!       
    games: [game!]!
    
  }  
  
  type game {
    x: Int!
    y: Int!
    direction: String!
    lastPosition: Int!
    map: [[Int!]!]! 
    catches: [String]
  }
  
  input createGameInput{
    catches: [String]
  }  
  
  input creaPokemonInput{
    pokemon: String!
  }
  type Catche{
    pokemon: String!
  }
`;


const BASE_URL = "https://0qh9zi3q9g.execute-api.eu-west-1.amazonaws.com/development";
const BASE_URL2 = "https://0qh9zi3q9g.execute-api.eu-west-1.amazonaws.com/development/collections";
//const APPLICATION_ID = "sergio.diez"
//no puedo ponerla porque me aparece como inaccesible AWS


const resolvers = {
  Query: {
    player: async (_, { name }) => {
      const { value } = await fetchJson(`${BASE_URL}/pairs/${name}`, {
        headers: { "x-application-id": APPLICATION_ID },
      });
      return JSON.parse(value);
    },

    allGames: async (_, { name }) => {
      //console.log("al lio"+name);
      return searchAllGamesByPrefix(name);
    },
    allPlayers: async () => {
      //console.log("a por esos players loco")
      return searchAllPlayers();
    }
  },
  Mutation: {
    creaplayer: async (_, args) => {
      const player = {
        gameId: 0,
        name: args.name,
        games: []
      };

      await fetch(`${BASE_URL}/pairs/${player.name}`, {
        body: JSON.stringify(player),
        method: "PUT",
        headers: {
          "x-application-id": APPLICATION_ID
        },
      })
      return player;
    },


    createGame: async (_, args) => {
      const player = args.input
      const nombre = player.name;
      const id = parseInt(player.gameId) + 1;
      const game = {
        x: args.x,
        y: args.y,
        direction: args.direction,
        lastPosition: args.lastPosition,
        map: args.map,
        catches: args.catches
      };


      await fetch(`${BASE_URL}/pairs/${nombre}-${id}`, {
        body: JSON.stringify(game),
        method: "PUT",
        headers: {
          "x-application-id": APPLICATION_ID
        },
      })
      return game;
    },

    // Add game to player
    updatePlayer: async (_, args) => {
      const player = args.input;
      const name = player.name;
      const numbGamePlayer = parseInt(player.gameId) + 1;
      const gameId = numbGamePlayer;
      args.input.gameId = numbGamePlayer;
      const url = BASE_URL + "/pairs/" + name + "-" + gameId;
      console.log(player.name),
        player.games = player.games.concat(url),

        await fetch(`${BASE_URL}/pairs/${player.name}`, {
          body: JSON.stringify(player),
          method: "PUT",
          headers: {
            "x-application-id": APPLICATION_ID,
          },
        })
      return player;
    },

    updateCatches: async (_, args) => {
      const pokemon = args.Catche;
      const game = args.input;
      const url = args.url;
      const name = pokemon.name;
      game.catches = game.catches.concat(pokemon),

        await fetch(`${url}`, {
          body: JSON.stringify(game),
          method: "PUT",
          headers: {
            "x-application-id": APPLICATION_ID,
          },
        })
      return game;
    },


  },
  player: {
    name: player => player.name,
    gameId: player => player.gameId,
    games: player => { return searchAllGamesByPrefix(player.name) }
  },

  game: {
    map: games => games.map,
    catches: games => games.catches
  },

  Catche: {
    pokemon: Catche => Catche.pokemon
  }
};

async function searchAllGamesByPrefix(prefix) {
  //console.log("fetch comming");
  const obj = await fetchJson(`${BASE_URL2}/${prefix}-`, {
    method: "GET",
    headers: {
      "x-application-id": APPLICATION_ID
    }
  });
  //console.log("fetch done");
  //console.log(obj);
  const list = [];
  // I go through the games object and put them in a list
  for (var i in obj) {
    //console.log("funca" + obj[i].value);
    list.push(JSON.parse(obj[i].value));
  }
  return list;
}

async function searchAllPlayers() {
  //console.log("Fetch all players");
  const obj = await fetchJson(`${BASE_URL}/pairs/`, {
    method: "GET",
    headers: {
      "x-application-id": APPLICATION_ID
    }
  });
  //console.log("Fetch done");
  //console.log(obj);
  const list = [];

  // I go through the players object and put them in a list
  for (var i in obj) {
    //console.log("funca" + obj[i].value);
    list.push(JSON.parse(obj[i].value));
  }
  return list;
}

const server = new GraphQLServer({ typeDefs, resolvers });

server.start({
  playground: "/",
  port: process.env.PORT || 5002
});