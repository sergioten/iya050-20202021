import React, { useContext, useState } from "react";
import AuthContext from "../AuthContext.js";
import 'regenerator-runtime/runtime';

const History = () => {
  const { user } = useContext(AuthContext);
  const [linksgames, setGames] = useState("");
  const fetchJson = (...args) => fetch(...args).then(response => response.json());


  async function getGames() {
    const stats = await fetchJson('https://entrega-ex-prograweb.glitch.me/', {
      method: 'POST',
      headers: {
        'content-type': 'application/json'
      },
      body: JSON.stringify({
        query: `
        query($name: String!){
          allGames(name: $name){
            x
            y
            direction
            lastPosition
            map
            catches
          }
        }`,
        variables: {
          name: user.name
        }
      })
    });
    console.log("Muestro games " + JSON.stringify(stats.data))
    setGames(JSON.stringify(stats.data));
    return JSON.stringify(stats.data);
  }

  return (
    <div><p>{user.name}’s past games</p>
      <p>{linksgames}</p>
      <input type="button" onClick={getGames} value="Ver Partidas"></input> </div>
  );


};

export default History;
