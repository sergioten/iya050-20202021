import React, { useContext, useState } from "react";
import AuthContext from "../AuthContext.js";
import 'regenerator-runtime/runtime';

const Stats = () => {
  const { user } = useContext(AuthContext);
  const [statsPlayer, setStatsPlayer] = useState("");
  const fetchJson = (...args) => fetch(...args).then(response => response.json());

  async function obtenEstadísticas() {
    const response = await fetchJson('https://entrega-ex-prograweb.glitch.me/', {
      method: 'POST',
      headers: {
        'content-type': 'application/json'
      },
      body: JSON.stringify({
        query: `
        query{
        allPlayers{
          name
          gameId
          games{
            x
            y
            direction
            lastPosition
            map
            catches
          }
        }
      }`
      })
    });

    setStatsPlayer(JSON.stringify(response.data.allPlayers));
    return JSON.stringify(response.data.allPlayers);
    
  }

  return (
    <div><p>{user.name}’s stats</p>
      <p>{statsPlayer}</p>
      <input type="button" onClick={obtenEstadísticas} value="Ver Stats"></input> </div>
  )
};

export default Stats;