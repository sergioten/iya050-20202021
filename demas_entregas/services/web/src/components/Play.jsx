import React, { useContext, useState, useEffect } from "react";
import AuthContext from "../AuthContext.js";
import 'regenerator-runtime/runtime';

const Play = () => {
  const { user } = useContext(AuthContext);
  const [playerObtained, setPlayerObtai] = useState("");
  const fetchJson = (...args) => fetch(...args).then(response => response.json());

  // HTTP request example to initially load data. See https://reactjs.org/docs/hooks-effect.html.
  useEffect(async () => {
    await obtenerEstado();
  }, []);


  // Example of a periodic HTTP request (every 5 seconds)
  useEffect(() => {
    const handle = setInterval(async () => {
      await updateFlower();
    }, 5000);

    // The returned function will be used to stop the `setInterval` if the component is unmounted (for example, because the user navigates to another page). See https://reactjs.org/docs/hooks-effect.html#effects-with-cleanup.
    return () => clearInterval(handle);
  }, []);



  useEffect(() => {
    window.addEventListener('keydown', handleKeyDown);
    // cleanup this component
    return () => {
      window.removeEventListener('keydown', handleKeyDown);
    };
  }, []);


  const handleKeyDown = async (event) => {
    if (event.key === "ArrowLeft" || event.key === "a") {
      await updateState("a");
      return <p>{playerObtained}</p>
    }
    if (event.key === "ArrowRight" || event.key === "d") {
      await updateState("d");
      return <p>{playerObtained}</p>
    }
    if (event.key === "ArrowUp" || event.key === "w") {
      await updateState("w");
      return <p>{playerObtained}</p>
    }
    if (event.key === "ArrowDown" || event.key === "s") {
      await updateState("s");
      return <p>{playerObtained}</p>
    }
  };

  async function updateState(keyPressed) {
    let url = `${process.env.CONTROLLER_SERVER_URL}/game/${user.name}/event`
    await fetchJson(`${url}`, {
      method: 'POST',
      headers: {
        'content-type': 'application/json'
      },
      body: JSON.stringify({
        keyPressed: keyPressed
      })
    }).then(response => {
      console.log("This is the answer after updating player status" + JSON.stringify(response))
      setPlayerObtai(JSON.stringify(response))
    })
  }

  async function updateFlower() {
    let url = `${process.env.CONTROLLER_SERVER_URL}/game/${user.name}/flower`
    await fetchJson(`${url}`, {
      method: 'POST',
      headers: {
        'content-type': 'application/json'
      }
    }).then(response => {
      setPlayerObtai(JSON.stringify(response))
    })
  }

  async function obtenerEstado() {
    await fetchJson(`${process.env.CONTROLLER_SERVER_URL}/game/${user.name}`, {
      method: 'GET',
    }).then(response => {
      console.log("This is the answer after getting the state of my game" + JSON.stringify(response))
      setPlayerObtai(JSON.stringify(response))
    })
  }

  return (
    <div>
      <div><p>Este es el estado de tu partida, {user.name} </p>
        <p>{playerObtained}</p>
        <input type="button" onClick={obtenerEstado} value="Estado game"></input> </div>
    </div>
  );
};

export default Play;