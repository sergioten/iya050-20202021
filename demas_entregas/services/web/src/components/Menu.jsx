import React, { useContext } from "react";
import { Link, Redirect, Route, Switch, useRouteMatch } from "react-router-dom";
import AuthContext from "../AuthContext.js";
import Stats from "./Stats.jsx";
import Play from "./Play.jsx";
import History from "./History.jsx";

const Menu = () => {
  const { isLoggedIn, user } = useContext(AuthContext);
  const { path, url } = useRouteMatch();
  const fetchJson = (...args) => fetch(...args).then(response => response.json());
  //var miplayer;

  if (!isLoggedIn) return <Redirect to="/sign-in" />;

  async function createNewGame() {
    await fetchJson(`${process.env.CONTROLLER_SERVER_URL}/game/`, {
      method: 'POST',
      headers: {
        'content-type': 'application/json'
      },
      body: JSON.stringify({
        playerName: user.name
      })
    }).then(response => {
      console.log("This is the answer after creating game for my player" + JSON.stringify(response))
    })
  }

  return (
    <>
      <nav>
        <ul>
          <li>
            <Link to={`${url}/stats`}>Stats</Link>
          </li>
          <li>
            <Link to={`${url}/play`} onClick={createNewGame}>New game</Link>
          </li>
          <li>
            <Link to={`${url}/history`}>Past games</Link>
          </li>
        </ul>
      </nav>
      <Switch>
        <Route exact path="/">
          <h2>Bienvenido/a, {user.name}</h2>
        </Route>

        <Route path={`${path}/stats`}>
          <Stats />
        </Route>

        <Route path={`${path}/play`}>
          <Play />
        </Route>

        <Route path={`${path}/history`}>
          <History />
        </Route>
      </Switch>
    </>
  );
};

export default Menu;
