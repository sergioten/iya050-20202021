const fetch = require("node-fetch");


const PROBABILIDAD_DE_Catche = 100;
const MAX_POKEMON_POKEAPI = 899;

//Indicate what each number represents in the array // Pokemon sprites front
const FLOWER1 = 1;
const GREENWALL1 = 3;
const HALFGRASSWALL1 = 4;
const GREYWALL1 = 5;
const HALFPATHWALL1 = 6;
const HIGHGRASS = 8;
const POSTER1 = 9;
const PLAYER = 11;
const FLOWER2 = 12;

//key movement pressed
const LEFT = -1;
const RIGHT = 1;
const UP = -1;
const DOWN = 1;


//keyPressed == W A S D or ↑ ← ↓ →
const movePlayer = async (keyPressed, currentMap, currentPlayer, currentCatches) => {
    let playerNext = {
        x: currentPlayer.x,
        y: currentPlayer.y,
        direction: currentPlayer.direction,
        lastPosition: currentPlayer.lastPosition
    };
    let catchesNext = currentCatches;
    let mapNext = currentMap;

    if (keyPressed === "ArrowLeft" || keyPressed === "a") {
        playerNext.x = playerNext.x + LEFT;
        playerNext.direction = 'left';
    }
    if (keyPressed === "ArrowRight" || keyPressed === "d") {
        playerNext.x = playerNext.x + RIGHT;
        playerNext.direction = 'right';
    }
    if (keyPressed === "ArrowUp" || keyPressed === "w") {
        playerNext.y = playerNext.y + UP;
        playerNext.direction = 'up';
    }
    if (keyPressed === "ArrowDown" || keyPressed === "s") {
        playerNext.y = playerNext.y + DOWN;
        playerNext.direction = 'down';
    }

    if (moveAllowed(currentMap, playerNext)) {
        playerNext.lastPosition = currentMap[playerNext.y][playerNext.x];
        if (tallGrass(currentMap, playerNext)) {
            const Catche = await catchRandomPokemon();
            if (Catche != null) {
                catchesNext.push(Catche);
            }
        }
    } else {
        playerNext.x = currentPlayer.x;
        playerNext.y = currentPlayer.y;
    }

    mapNext[currentPlayer.y][currentPlayer.x] = currentPlayer.lastPosition;
    mapNext[playerNext.y][playerNext.x] = PLAYER;

    return { mapNext, playerNext, catchesNext };
}


// Check if the player can be moved to the new position. Returns true or false.
const moveAllowed = (currentMap, playerNext) => {
    // First it checks if the values ​​do not go out of the map and then it checks if it is walkable to the element of the Next position
    if (playerNext.x >= 0 && playerNext.y >= 0 &&
        // If x is less than or equal to the number of columns -1
        playerNext.x < currentMap[0].length &&
        // If the y is less than or equal to the number of rows -1
        playerNext.y < currentMap.length &&
        currentMap[playerNext.y][playerNext.x] != GREENWALL1 &&
        currentMap[playerNext.y][playerNext.x] != HALFGRASSWALL1 &&
        currentMap[playerNext.y][playerNext.x] != GREYWALL1 &&
        currentMap[playerNext.y][playerNext.x] != HALFPATHWALL1 &&
        currentMap[playerNext.y][playerNext.x] != POSTER1
    ) {
        return true;
    }
    return false;
}


// Check if the space to move to is tall grass. Returns true or false.
const tallGrass = (currentMap, playerNext) => {
    if (currentMap[playerNext.y][playerNext.x] == HIGHGRASS) {
        return true;
    }
    return false;
}

// A % chance that when moving into tall grass, a random Pokemon will be captured
const catchRandomPokemon = async () => {
    if (Math.floor(Math.random() * (101 - 1) + 1) <= PROBABILIDAD_DE_Catche) {
        const id = Math.floor(Math.random() * (MAX_POKEMON_POKEAPI - 1) + 1);
        let capturedPokemon;
        await fetch(`https://pokeapi.co/api/v2/pokemon/${id}`)
            .then(res => res.json())
            .then(pokemon => {
                capturedPokemon = pokemon.name;
            });
        return capturedPokemon;
    }
    return null;
}

const moveFlowers = (currentMap, currentPlayer) => {
    let mapNext = currentMap;
    let playerNext = currentPlayer;
    for (let y = 0; y < currentMap.length; y++) {
        for (let x = 0; x < currentMap[y].length; x++) {
            if (currentMap[y][x] === FLOWER1) {
                mapNext[y][x] = FLOWER2;
                if (currentPlayer.lastPosition === FLOWER1) {
                    playerNext.lastPosition = FLOWER2;
                }
            } else if (currentMap[y][x] === FLOWER2) {
                mapNext[y][x] = FLOWER1;
                if (currentPlayer.lastPosition === FLOWER2) {
                    playerNext.lastPosition = FLOWER1;
                }
            }
        }
    }
    return { mapNext, playerNext };
}

module.exports = { movePlayer, moveFlowers };