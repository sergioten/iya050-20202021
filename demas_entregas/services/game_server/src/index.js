const express = require("express");
const cors = require("cors");
const pokemonRoutes = require("./routes/pokemon.js");

const server = express();

// It's like using body-parser, but without the 'deprecated' message
server.use(express.urlencoded({
    extended: true
}));
server.use(express.json());
server.use(cors());
server.get("/hello", (req, res) => {
    res.send("Bienvenido/a a la PokeAPI");
});

server.use("/pokemon", pokemonRoutes);


server.listen(process.env.PORT || 5001);
