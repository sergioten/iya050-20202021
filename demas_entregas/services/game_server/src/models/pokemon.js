//Game Map
let gameMap = [
    [2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2],
    [2, 3, 3, 3, 3, 3, 7, 7, 3, 2, 2, 2, 2, 2, 2, 2, 2, 9, 3, 2],
    [2, 3, 2, 2, 2, 3, 7, 7, 3, 3, 3, 3, 5, 5, 5, 5, 5, 5, 3, 2],
    [2, 5, 2, 1, 1, 3, 7, 7, 3, 2, 2, 2, 2, 8, 8, 8, 8, 8, 5, 2],
    [2, 3, 2, 2, 2, 3, 7, 7, 3, 1, 1, 1, 1, 8, 8, 8, 8, 8, 3, 2],
    [2, 5, 2, 2, 2, 3, 7, 7, 3, 2, 9, 2, 2, 2, 2, 2, 2, 2, 5, 2],
    [2, 5, 4, 4, 10, 3, 7, 7, 3, 2, 2, 2, 2, 2, 1, 1, 1, 2, 5, 2],
    [2, 5, 4, 4, 4, 4, 6, 6, 7, 7, 7, 7, 7, 6, 6, 6, 6, 6, 6, 6],
    [2, 5, 2, 2, 2, 3, 7, 7, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 5, 2],
    [2, 5, 2, 2, 2, 2, 7, 7, 3, 4, 10, 10, 4, 4, 4, 4, 4, 4, 5, 4],
    [2, 5, 2, 1, 1, 2, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 11, 7],
    [2, 3, 2, 2, 2, 2, 2, 2, 2, 1, 1, 2, 2, 8, 8, 8, 8, 8, 3, 2],
    [2, 3, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 8, 8, 8, 8, 8, 3, 2],
    [2, 3, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 3, 2],
    [2, 3, 8, 8, 8, 8, 2, 2, 2, 1, 1, 1, 2, 8, 8, 8, 8, 8, 3, 2],
    [2, 3, 3, 3, 3, 3, 7, 7, 3, 2, 2, 2, 2, 2, 2, 2, 2, 9, 3, 2],
];

const FLOWER1 = 1;
const GRASS1 = 2;
const GREENWALL1 = 3;
const HALFGRASSWALL1 = 4;
const GREYWALL1 = 5;
const HALFPATHWALL1 = 6;
const PATH1 = 7;
const HIGHGRASS = 8;
const POSTER1 = 9;
const WALLPATH1 = 10;
const PLAYER = 11;
const FLOWER2 = 12;

//Player position and vision
let player = {
    x: 18,
    y: 9,
    direction: 'left',
    lastPosition: 7
};

let catches = [];

const getPlayer = () => player;
const setPlayer = newPlayer => {
    player = newPlayer;
};

const getCatches = () => catches;
const setCatches = newCatches => {
    catches = newCatches;
};

const setAll = ({ newGameMap, newPlayer, newCatches }) => {
    gameMap = newGameMap;
    player = newPlayer;
    catches = newCatches;
}

const getMap = () => gameMap;
const setMap = newGameMap => {
    gameMap = newGameMap;
};

const getAll = () => {
    { gameMap, player, catches }
}



module.exports = { getAll, setAll, getMap, setMap, getPlayer, setPlayer, getCatches, setCatches };