const pokemonAPIGame = require("../services/pokemonAPIGame.js");


// It receives the key pressed by the player to move and it must also receive {gameMap, player, catches}
const next = async (req, res) => {
  res.json(await pokemonAPIGame.movePlayer(req.body.keyPressed, req.body.currentMap,
    req.body.currentPlayer, req.body.currentCatches));
};

const nextFlower = (req, res) => {
  res.json(pokemonAPIGame.moveFlowers(req.body.currentMap, req.body.currentPlayer));
};

module.exports = { next, nextFlower };