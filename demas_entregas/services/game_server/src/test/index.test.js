const { assert } = require('chai');
const fetch = require('node-fetch');

const urlNext = 'http://localhost:4000/pokemon/next';
const urlNextFlower = 'http://localhost:4000/pokemon/nextFlower';
const urlHello = 'http://localhost:4000/hello';

const currentMap = [
    [2, 3, 3, 3, 3, 3, 7, 7, 3, 2, 2, 2, 2, 2, 2, 2, 2, 9, 3, 2],
    [2, 3, 2, 2, 2, 3, 7, 7, 3, 3, 3, 3, 5, 5, 5, 5, 5, 5, 3, 2],
    [2, 3, 2, 2, 2, 3, 7, 7, 3, 1, 1, 1, 1, 8, 8, 8, 8, 8, 3, 2],
    [2, 5, 2, 1, 1, 3, 7, 7, 3, 2, 2, 2, 2, 8, 8, 8, 8, 8, 5, 2],
    [2, 5, 2, 2, 2, 3, 7, 7, 3, 2, 2, 2, 2, 8, 8, 8, 8, 8, 5, 2],
    [2, 5, 2, 2, 2, 3, 7, 7, 3, 2, 9, 2, 2, 2, 2, 2, 2, 2, 5, 2],
    [2, 5, 4, 4, 10, 3, 7, 7, 3, 2, 2, 2, 2, 2, 1, 1, 1, 2, 5, 2],
    [2, 5, 2, 2, 2, 3, 7, 7, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 5, 2],
    [2, 5, 2, 2, 2, 2, 7, 7, 3, 4, 10, 10, 4, 4, 4, 4, 4, 4, 5, 4],
    [2, 5, 2, 1, 1, 2, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7],
    [2, 5, 4, 4, 4, 4, 6, 6, 7, 7, 7, 7, 7, 6, 6, 6, 6, 6, 6, 6],
    [2, 3, 2, 2, 2, 2, 2, 2, 2, 1, 1, 2, 2, 8, 8, 8, 8, 8, 3, 2],
    [2, 3, 11, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 8, 8, 8, 8, 8, 3, 2],
    [2, 3, 8, 8, 8, 8, 2, 2, 2, 1, 1, 1, 2, 8, 8, 8, 8, 8, 3, 2],
    [2, 3, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 3, 2],
    [2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2],
];

const currentPlayer = {
    x: 2,
    y: 12,
    direction: 'down',
    lastPosition: 1
};
const currentCatches = ["pikachu"];
const keyPressed = "d";

const data = { keyPressed, currentMap, currentPlayer, currentCatches };

describe('NExt Game state', () => {
    it('Must return ok', async () => {
        const response = await fetch(urlNext, {
            method: 'POST',
            headers: { 'content-type': 'application/json', },
            body: JSON.stringify(data)
        });
        assert(response.ok);
    });
    it('Must return game state', async () => {
        const response = await fetch(urlNext, {
            method: 'POST',
            headers: { 'content-type': 'application/json', },
            body: JSON.stringify(data)
        });
        response.ok
        const body = await response.json();
        assert.isNotNull(body.mapNext);
    });
});

describe('Next flowers state', () => {
    it('Must return ok', async () => {
        const response = await fetch(urlNextFlower, {
            method: 'POST',
            headers: { 'content-type': 'application/json', },
            body: JSON.stringify(data)
        });
        assert(response.ok);
    });
    it('Must return map', async () => {
        const response = await fetch(urlNextFlower, {
            method: 'POST',
            headers: { 'content-type': 'application/json', },
            body: JSON.stringify(data)
        });
        response.ok
        const body = await response.json();
        assert.isNotNull(body.mapNext);
    });
});

describe('Hi server', () => {
    it('Must return ok', async () => {
        const response = await fetch(urlHello, {
            method: 'GET',
        });
        assert(response.ok);
    });
});
