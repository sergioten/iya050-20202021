const express = require("express");
const pokemonController = require("../controllers/pokemon.js");

const router = express.Router();


router.post("/next", pokemonController.next);
router.post("/nextFlower", pokemonController.nextFlower)


module.exports = router;