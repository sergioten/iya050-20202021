const Koa = require("koa");
const koaBody = require("koa-body");
const cors = require("@koa/cors");
const Router = require("@koa/router");
const fetch = require("node-fetch");

const fetchJson = (...args) => fetch(...args).then(response => response.json());

let inputPlayer;
let persistenceState = {
  x: 18,
  y: 9,
  direction: 'left',
  lastPosition: 7,
  map: [[]],
  catches: []
}
let nextState;
let playerName;
let newMap = [
  [2, 3, 3, 3, 3, 3, 7, 7, 3, 2, 2, 2, 2, 2, 2, 2, 2, 9, 3, 2],
  [2, 3, 2, 2, 2, 3, 7, 7, 3, 3, 3, 3, 5, 5, 5, 5, 5, 5, 3, 2],
  [2, 3, 2, 2, 2, 3, 7, 7, 3, 1, 1, 1, 1, 8, 8, 8, 8, 11, 3, 2],
  [2, 3, 8, 8, 8, 8, 2, 2, 2, 1, 1, 1, 2, 8, 8, 8, 8, 8, 3, 2],
  [2, 3, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 3, 2],
  [2, 5, 2, 2, 2, 3, 7, 7, 3, 2, 9, 2, 2, 2, 2, 2, 2, 2, 5, 2],
  [2, 5, 4, 4, 10, 3, 7, 7, 3, 2, 2, 2, 2, 2, 1, 1, 1, 2, 5, 2],
  [2, 5, 2, 2, 2, 3, 7, 7, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 5, 2],
  [2, 5, 2, 2, 2, 2, 7, 7, 3, 4, 10, 10, 4, 4, 4, 4, 4, 4, 5, 4],
  [2, 5, 2, 1, 1, 2, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7],
  [2, 5, 4, 4, 4, 4, 6, 6, 7, 7, 7, 7, 7, 6, 6, 6, 6, 6, 6, 6],
  [2, 3, 2, 2, 2, 2, 2, 2, 2, 1, 1, 2, 2, 8, 8, 8, 8, 8, 3, 2],
  [2, 5, 2, 1, 1, 3, 7, 7, 3, 2, 2, 2, 2, 8, 8, 8, 8, 8, 5, 2],
  [2, 5, 2, 2, 2, 3, 7, 7, 3, 2, 2, 2, 2, 8, 8, 8, 8, 8, 5, 2],
  [2, 3, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 8, 8, 8, 8, 8, 3, 2],
  [2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2],
];

let newPlayer = {
  x: 17,
  y: 2,
  direction: 'left',
  lastPosition: 8
};

let currentMap = [[]];
let currentPlayer = {
  x: -1,
  y: -1,
  direction: '-1',
  lastPosition: -1
}
let currentCatches = [];

const app = new Koa();
const router = new Router();

// Post mutation to Stats server to store new game
router.post("/game", async (ctx) => {
  playerName = ctx.request.body;
  await createNewGame();
  // Return state
  console.log("Mi estado " + JSON.stringify(persistenceState));

  ctx.response.set("Content-Type", "application/json");
  ctx.body = JSON.stringify({
    id: ctx.params.id,
    persistenceState
  });
});

  // Get current state from Stats server
router.get("/game/:id", async (ctx) => {
  playerName = ctx.params.id;
  await getPlayerById();
  // Return state

  ctx.response.set("Content-Type", "application/json");
  ctx.body = JSON.stringify({
    //id: playerName,
    id: ctx.params.id,
    persistenceState
  });
});

  // Get current state from Stats server
router.post("/game/:id/event", async (ctx) => {
  playerName = ctx.params.id;
  let keyPressed = ctx.request.body.keyPressed;
  await getPlayerById();
  currentMap = persistenceState.map;
  currentPlayer.x = persistenceState.x;
  currentPlayer.y = persistenceState.y;
  currentPlayer.direction = persistenceState.direction;
  currentPlayer.lastPosition = persistenceState.lastPosition;
  currentCatches = persistenceState.catches;

  await getNextStateFromGameServer(keyPressed);
  //console.log("ESTADO NEXT " + nextState.playerNext.x);
  // 3. post mutation to Stats server to store next state
  await createGameNextState();
  //console.log("catchesNEXT" + persistenceState.catches)
  //await actualizacatches();
  // 4. return state
  ctx.response.set("Content-Type", "application/json");
  ctx.body = JSON.stringify({
    id: ctx.params.id,
    persistenceState
  });
});

router.post("/game/:id/flower", async (ctx) => {
  // 1. get current state from Stats server
  playerName = ctx.params.id;
  await getPlayerById();
  currentMap = persistenceState.map;
  currentPlayer.x = persistenceState.x;
  currentPlayer.y = persistenceState.y;
  currentPlayer.direction = persistenceState.direction;
  currentPlayer.lastPosition = persistenceState.lastPosition;
  currentCatches = persistenceState.catches;

  //console.log("map " + currentMap + " player " + currentPlayer.x + " stats " + currentCatches)

  // Get next state from Game server
  await getNextStateFromGameServerFlower();

  // Post mutation to Stats server to store next state
  await createGameNextStateFlower();
  
  // Return state
  ctx.response.set("Content-Type", "application/json");
  ctx.body = JSON.stringify({
    id: ctx.params.id,
    persistenceState
  });
});


//Crea game nueva
async function createNewGame() {
  await catchPlayer();
  await fetchJson(`${process.env.STATS_SERVER_URL}`, {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify({
      query: `
      mutation($input: createplayerInput, $map: [[Int!]!]!, $x: Int!, $y: Int!, $direction: String!, $lastPosition: Int!, $catches: [String]){
        createGame(input: $input, map: $map, x: $x, y: $y, direction: $direction, lastPosition: $lastPosition, catches: $catches){
          x
          y
          direction
          lastPosition
          map
          catches
        }
      }`,
      variables: {
        input: {
          name: inputPlayer.name,
          gameId: inputPlayer.gameId,
          games: []
        },
        map: newMap,
        x: newPlayer.x,
        y: newPlayer.y,
        direction: newPlayer.direction,
        lastPosition: newPlayer.lastPosition,
        catches: []
      }
    })
  }).then(response => {
    persistenceState.x = response.data.createGame.x;
    persistenceState.y = response.data.createGame.y;
    persistenceState.direction = response.data.createGame.direction;
    persistenceState.lastPosition = response.data.createGame.lastPosition;
    persistenceState.map = response.data.createGame.map;
    persistenceState.catches = response.data.createGame.catches;
  })
}

//Get player with name
async function catchPlayer() {
  await fetchJson(`${process.env.STATS_SERVER_URL}`, {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify({
      query: `
    query($name: String!){
      player(name: $name){
        name
        gameId        
        games{
          x
          y
          direction
          lastPosition
          map
          catches
        }
      }
    }`,
      variables: {
        name: playerName.playerName
      }
    })
  }).then(response => {
    inputPlayer = response.data.player;

  })
}

//Get player with name
async function getPlayerById() {
  await fetchJson(`${process.env.STATS_SERVER_URL}`, {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify({
      query: `
    query($name: String!){
      player(name: $name){
        name
        gameId        
        games{
          x
          y
          direction
          lastPosition
          map
          catches
        }
      }
    }`,
      variables: {
        name: playerName
      }
    })
  }).then(response => {
    inputPlayer = response.data.player;
    persistenceState.x = response.data.player.games[0].x;
    persistenceState.y = response.data.player.games[0].y;
    persistenceState.direction = response.data.player.games[0].direction;
    persistenceState.lastPosition = response.data.player.games[0].lastPosition;
    persistenceState.map = response.data.player.games[0].map;
    persistenceState.catches = response.data.player.games[0].catches;
  })
}

async function getNextStateFromGameServer(keyPressed) {
  await fetchJson(`${process.env.GAME_SERVER_URL}/pokemon/next`, {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify({
      keyPressed: keyPressed,
      currentMap: currentMap,
      currentPlayer: currentPlayer,
      currentCatches: currentCatches
    })
  }).then(response => {
    console.log("Respuesta tras el nuevo estado de la game" + JSON.stringify(response))
    nextState = response;
  })
}

async function getNextStateFromGameServerFlower() {
  await fetchJson(`${process.env.GAME_SERVER_URL}/pokemon/nextFlower`, {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify({
      currentMap: currentMap,
      currentPlayer: currentPlayer
    })
  }).then(response => {
    nextState = response;
  })
}

async function createGameNextState() {
  await getPlayerById();
  await fetchJson(`${process.env.STATS_SERVER_URL}`, {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify({
      query: `
      mutation($input: crearInputPlayer, $map: [[Int!]!]!, $x: Int!, $y: Int!, $direction: String!, $lastPosition: Int!, $catches: [String]){
        createGame(input: $input, map: $map, x: $x, y: $y, direction: $direction, lastPosition: $lastPosition, catches: $catches){
          x
          y
          direction
          lastPosition
          map
          catches
        }
      }`,
      variables: {
        input: {
          name: inputPlayer.name,
          gameId: inputPlayer.gameId,
          games: []
        },
        map: nextState.mapNext,
        x: nextState.playerNext.x,
        y: nextState.playerNext.y,
        direction: nextState.playerNext.direction,
        lastPosition: nextState.playerNext.lastPosition,
        catches: nextState.catchesNext
      }
    })
  }).then(response => {
    persistenceState.x = response.data.createGame.x;
    persistenceState.y = response.data.createGame.y;
    persistenceState.direction = response.data.createGame.direction;
    persistenceState.lastPosition = response.data.createGame.lastPosition;
    persistenceState.map = response.data.createGame.map;
    persistenceState.catches = response.data.createGame.catches;
  })
}


async function createGameNextStateFlower() {
  await getPlayerById();
  await fetchJson(`${process.env.STATS_SERVER_URL}`, {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify({
      query: `
      mutation($input: crearInputPlayer, $map: [[Int!]!]!, $x: Int!, $y: Int!, $direction: String!, $lastPosition: Int!, $catches: [String]){
        createGame(input: $input, map: $map, x: $x, y: $y, direction: $direction, lastPosition: $lastPosition, catches: $catches){
          x
          y
          direction
          lastPosition
          map
          catches
        }
      }`,
      variables: {
        input: {
          name: inputPlayer.name,
          gameId: inputPlayer.gameId,
          games: []
        },
        map: nextState.mapNext,
        x: nextState.playerNext.x,
        y: nextState.playerNext.y,
        direction: nextState.playerNext.direction,
        lastPosition: nextState.playerNext.lastPosition,
        catches: persistenceState.catches
      }
    })
  }).then(response => {
    persistenceState.x = response.data.createGame.x;
    persistenceState.y = response.data.createGame.y;
    persistenceState.direction = response.data.createGame.direction;
    persistenceState.lastPosition = response.data.createGame.lastPosition;
    persistenceState.map = response.data.createGame.map;
  })
}

app.use(koaBody());
app.use(cors());
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(process.env.PORT || 5000);
