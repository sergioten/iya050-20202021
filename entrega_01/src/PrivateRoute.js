import React, { useContext } from 'react';
import { Route, Redirect } from 'react-router-dom';



export default function PrivateRoute(props) {

    const token = localStorage.getItem('token')
    const { component: Component, ...rest } = props;

    if(token){
        return ( 
            <Route {...rest} render = {
                (props) => (
                    <Component {...props} />
                )
            }/>
        )}

    return <Redirect to='/login' />
}