/* How to sign in using johndoe's credentials:
    POST /users/johndoe/sessions HTTP/1.1
    [...]{ 
      "password": "correcthorsebatterystaple" 
    } */

import React, { useState } from "react";
import { Link, Redirect } from "react-router-dom";
import Axios from 'axios';
import logoImg from "../img/logo-uneatlantico.png";
import { Card, Logo, Form, Input, Button } from "../components/AuthForms";
import 'toastr/build/toastr.css';
import toastr from 'toastr';

export default function Login() {
  const [isLoggedIn, setLoggedIn] = useState(false);
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");

  function postLogin() {
    Axios.post("https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/users/" + userName + "/sessions/", {
      password
    }).then(result => {
      if (result.status === 201) {
        toastr.success("Conexion Establecida"); 
        toastr.success("Login correcto!"); 
        localStorage.setItem("token", result.data.sessionToken);
        setLoggedIn(true);
      }      
    }).catch(e => {//Toaster error
      toastr.error(e);
    });
  }

  if (isLoggedIn) {
    return <Redirect to="/game" />;
  }

  return (
    <Card>
      <Logo src={logoImg} />
      <Form>
        
        <Input type="username" value={userName} onChange={e => {
            setUserName(e.target.value);
          }} placeholder="Nombre de usuario"/>
        
        <Input type="password" value={password} onChange={e => {
            setPassword(e.target.value);
          }} placeholder="Contraseña"/>
        
        <Button onClick={postLogin}>Acceder</Button>
      
      </Form>
      <Link to="/signup">¿Aún no tienes cuenta? Regístrate</Link>
    </Card>
  );
}