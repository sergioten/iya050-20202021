import React from "react";
import homeImgLogo from "../img/Escudo_UNEATLANTICO.jpg";
import { Imagen, Title } from '../components/AuthForms';

export default function Home() {

  return (
    <div>
      <div>
        <Title>
          <h1>ENTREGA_01 | PROGRAMACIÓN WEB II | 2020-2021</h1>
        </Title>
      </div>

      <Imagen src={homeImgLogo} width="321" height="96"/>
      <div>
        <h3>Sergio Díez Fernández</h3>
      </div> 
    </div>
    
  );

}