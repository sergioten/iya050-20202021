import styled from 'styled-components';

const Title = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  width: 100%;
  text-align: center;
  margin-bottom: 30px;
`;

const Card = styled.div`
  box-sizing: border-box;
  max-width: 410px;
  margin: 0 auto;
  padding: 0 2rem;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const CardLeft = styled.div`
  box-sizing: border-box;
  max-width: 410px;
  margin: left;
  padding: 0 2rem;
  display: flex;
  flex-direction: column;
  align-items: left;
`;

const Form = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;


const CenterDiv = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  width: 100%;
  text-align: center;
  margin-bottom: 20px;
`;

const Input = styled.input`
  padding: 1rem;
  border: 1px solid #999;
  margin-bottom: 1rem;
  font-size: 0.8rem;
`;

const Button = styled.button`
  background: linear-gradient(to bottom, #360033, #360033);
  border-color: #3f4eae;
  border-radius: 1px;
  padding: 1rem;
  color: white;
  font-weight: 700;
  width: 100%;
  margin-bottom: 1rem;
  font-size: 0.8rem;
`;

const Logo = styled.img`
  width: 60%;
  margin-bottom: 1rem;
  width: 321;
  height: 96;
`;

const Imagen = styled.img`
  max-width: 100%;
  height: auto;
  display: block;
  margin-left: auto;
  margin-right: auto;
`;
const Error = styled.div`
  background-color: red;
`;

export { Title, Form, Input, Error, Imagen, Button, Logo, Card, CenterDiv, CardLeft };